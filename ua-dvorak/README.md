# Ukrainian Dvorak

this keyboard layout puts Ukrainian letters to their English counterparts of the standard Dvorak layout. this correspondence is phonetic. this aims to enable Dvorak users to only have to remember a single layout for both Latin and Cyrillic scripts.

some punctuation marks and extra Cyrillic letters were added for convenience.

![](showcase.png)

## Q: I see some keys are assigned two letters at once, how does this work?

similarly to how holding `Shift` switches keys to their 2nd level (typically used for the upper case), more levels can be accessed with more modifiers. one such modifier is `AltGr` (`RAlt` on some keyboards). using `AltGr` and `Shift`+`AltGr` for 3rd and 4th levels respectively makes it possible to assign 4 different symbols to a single key.

## Q: but Dvorak is for English, is it optimised for Ukrainian?

a brief overview of statistics seems to show that the original logic behind Dvorak still holds. the letters seem to be decently arranged in terms of how frequently they are used. this is lucky, as the intent of this project was to replicate Dvorak as close as possible and moving letters around was not an option.

